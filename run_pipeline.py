# Databricks notebook source
# MAGIC %md # Run common Libraries and creds

# COMMAND ----------

# MAGIC %sh
# MAGIC /databricks/python3/bin/python -m pip install --upgrade pip
# MAGIC pip install fsspec

# COMMAND ----------

# MAGIC %run /Shared/ML_Common/ML_InstallAndImport

# COMMAND ----------

# MAGIC %run /Shared/ML_Common/ML_CredentialObjects

# COMMAND ----------

sql_creds = SQLCreds(
  "env-sql-server-name", 
  "env-sql-database-name", 
  "synapse-analytics-sqlu", 
  "synapse-analytics-sqlp"
) 
storage_creds = StorageCreds(
  "Main Storage Creds",
  "clf-storage-blob-name",
  "clf-storage-blob-key"
)
login = WebLogin(
  "sh-user",
  "sh-password",
  "ah-user",
  "ah-password",
  "gold-user",
  "gold-password"  
)

# COMMAND ----------

# MAGIC %md #Create attributes dictionary

# COMMAND ----------

creds = {'server': sql_creds.server(), #'sql-wus-mlxi-qa.database.windows.net',
'database':sql_creds.db(),             #'insight_mart'
'username':sql_creds.user(),           #'mladmin'
'password': sql_creds.password(),      #'reFocusstarry9#'
'target_table': 'self_hosted_history1'}

blob_creds = {'storage_account':storage_creds.storage(),
              "storage_key":storage_creds.key(),
              "container":"self-hosted"}

weblogin = {'txtLogin': login.user(),
            'txtPassword': login.password(),
            'ah_txtLogin': login.ah_user(),
            'ah_txtPassword': login.ah_password(),
            'gold_txtLogin': login.gold_user(),
            'gold_txtPassword': login.gold_password(),
            'cmdLogin': 'Login'}

timeout = {'timeout':100}

attr_dict = {**creds, **weblogin, **blob_creds, **timeout}

attr_dict

# COMMAND ----------

# MAGIC %md #Run pipeline daily

# COMMAND ----------

dbutils.notebook.run('self_v4_history', 100000, attr_dict) #self_v2 _importfromprod
dbutils.notebook.exit("run successful") 

# COMMAND ----------

# MAGIC %md # Run pipeline for 1st of month

# COMMAND ----------

"""import datetime
now = datetime.datetime.now()
if now.day == 1:
  dbutils.notebook.run('self_v4', 100000, attr_dict)
  dbutils.notebook.exit("run successful")"""

# COMMAND ----------


