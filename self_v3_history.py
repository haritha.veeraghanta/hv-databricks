# Databricks notebook source
# MAGIC %md #import library

# COMMAND ----------

import requests
import pandas as pd
import numpy as np
import pyodbc
from bs4 import BeautifulSoup as bs
import os
from datetime import datetime, timezone, timedelta
import requests
import io
from io import StringIO
import pyspark
from pyspark import SparkConf, SparkContext
from pyspark.sql import SparkSession
from pyspark.sql.functions import lit
from requests.exceptions import Timeout

# COMMAND ----------

# MAGIC %md # declare and assign widget

# COMMAND ----------

# declare variables used in the pipeline

dbutils.widgets.text("database", "")
dbutils.widgets.text("server", "")
dbutils.widgets.text("username", "")
dbutils.widgets.text("password", "")
dbutils.widgets.text("target_table", "")

dbutils.widgets.text("storage_account", "")
dbutils.widgets.text("storage_key", "")
dbutils.widgets.text("container", "")

dbutils.widgets.text("txtLogin", "")
dbutils.widgets.text("txtPassword", "")
dbutils.widgets.text("cmdLogin", "")

# special login creds
dbutils.widgets.text("ah_txtLogin", "")
dbutils.widgets.text("ah_txtPassword", "")

dbutils.widgets.text("gold_txtLogin", "")
dbutils.widgets.text("gold_txtPassword", "")

dbutils.widgets.text("timeout","")

# COMMAND ----------

# assign variables from the master run script

database = dbutils.widgets.get("database")
server1 = dbutils.widgets.get("server")
username = dbutils.widgets.get("username")
password = dbutils.widgets.get("password")
table = dbutils.widgets.get("target_table")

st_account = dbutils.widgets.get("storage_account")
st_key = dbutils.widgets.get("storage_key")
st_container = dbutils.widgets.get("container")

webuser = dbutils.widgets.get("txtLogin")
webpwd = dbutils.widgets.get("txtPassword")
#weblogin = dbutils.widgets.get("cmdLogin")

#special login creds
ah_user = dbutils.widgets.get("ah_txtLogin")
ah_pwd = dbutils.widgets.get("ah_txtPassword")

gold_user = dbutils.widgets.get("gold_txtLogin")
gold_pwd = dbutils.widgets.get("gold_txtPassword")
time_out = int(dbutils.widgets.get("timeout"))

# COMMAND ----------

# connect to azure blob
#spark.conf.set("fs.azure.account.key.<your-storage-account-name>.blob.core.windows.net","<your-storage-account-access-key>")

# set the config to read_write
spark.conf.set("fs.azure.account.key.{0}.blob.core.windows.net".format(st_account),st_key)

output_container_path = "wasbs://%s@%s.blob.core.windows.net" % (st_container, st_account)
print(output_container_path)
output_blob_folder = "%s/self-hosted" % output_container_path
print(output_blob_folder)

# COMMAND ----------

default_login = {'txtLogin':webuser,'txtPassword':webpwd,'cmdLogin':'Login'}
# special login
ah_login = {'txtLogin': ah_user, 'txtPassword': ah_pwd, 'cmdLogin':'Login'}
gold_login = {'txtLogin': gold_user, 'txtPassword': gold_pwd, 'cmdLogin': 'Login'}

# COMMAND ----------

server =  server1.replace(".privatelink","")

jdbcUrl = f"jdbc:sqlserver://{server}.database.windows.net:1433;database={database}"
jdbcDriver = "com.microsoft.sqlserver.jdbc.SQLServerDriver"

# COMMAND ----------

print(database, server, username, password, webuser, webpwd, ah_user, ah_pwd, gold_user, gold_pwd, st_account, st_key, st_container)

# COMMAND ----------

# MAGIC %md # mount azure blob container and read master schema into df

# COMMAND ----------

# check if mount is present:
def sub_unmount(str_path):
    if any(mount.mountPoint == str_path for mount in dbutils.fs.mounts()):
        dbutils.fs.unmount(str_path)

        
# no need to run for the test, it is already mounted
'''if (sub_unmount('/mnt/self-hosted'),True):
  # mount storage to cluster
  dbutils.fs.mount(
    source = 'wasbs://{0}@{1}.blob.core.windows.net'.format(st_container,st_account),
    mount_point = '/mnt/self-hosted',
    extra_configs = {'fs.azure.account.key.{0}.blob.core.windows.net'.format(st_account):st_key})

print(st_account,st_key,st_container)'''

# COMMAND ----------

# master_billing csv is provided as the one with superset of columns includes bill month and bill year.

df = spark.read.csv('/mnt/self-hosted/master_billing.csv', header=True)

# COMMAND ----------

# MAGIC %md # functions

# COMMAND ----------

def get_hidden(html,weblogin):
  soup = bs(html,'html.parser')
  event_target_tag = soup.find_all(attrs={'id':'__EVENTTARGET'})
  event_argument_tag = soup.find_all(attrs={'id':"__EVENTARGUMENT"})
  view_state_tag = soup.find_all(attrs={'id':"__VIEWSTATE"})
  view_state_gen_tag = soup.find_all(attrs={'id':"__VIEWSTATEGENERATOR"})
  view_state_encrypt_tag = soup.find_all(attrs={'id':"__VIEWSTATEENCRYPTED"})
  event_validation_tag = soup.find_all(attrs={'id':"__EVENTVALIDATION"})
    
  # get values from tags for hidden
  #event_target_output = event_target_tag[0]['value']
  #event_argument_output = event_argument_tag[0]['value']
  view_state_output = view_state_tag[0]['value']
  view_state_gen_output = view_state_gen_tag[0]['value']
  view_state_encrypt_output = view_state_encrypt_tag[0]['value']
  event_validation_output = event_validation_tag[0]['value']
  
  hidden_params = {'__EVENTTARGET':"", "__EVENTARGUMENT":"", "__VIEWSTATE":f'{view_state_output}', "__VIEWSTATEGENERATOR":f'{view_state_gen_output}', "__VIEWSTATEENCRYPTED":f'{view_state_encrypt_output}', "__EVENTVALIDATION":f'{event_validation_output}'}
  
  hidden_params.update(weblogin)
  print("hidden + login params:", hidden_params)
  return hidden_params

# COMMAND ----------

def get_hidden2(html,weblogin, month, year):
    event_target_tag = invoice_soup.find_all(attrs={'id':'__EVENTTARGET'})
    event_argument_tag = invoice_soup.find_all(attrs={'id':"__EVENTARGUMENT"})
    view_state_tag = invoice_soup.find_all(attrs={'id':"__VIEWSTATE"})
    view_state_gen_tag = invoice_soup.find_all(attrs={'id':"__VIEWSTATEGENERATOR"})
    view_state_encrypt_tag = invoice_soup.find_all(attrs={'id':"__VIEWSTATEENCRYPTED"})
    event_validation_tag = invoice_soup.find_all(attrs={'id':"__EVENTVALIDATION"})
    month_tag = invoice_soup.find_all(attrs= {'id' : '_ctl0:bc:Month'})
    year_tag = invoice_soup.find_all(attrs= {'id': '_ctl0:bc:Year'})
    org_sortdir_tag = invoice_soup.find_all(attrs={'id': '_ctl0:bc:dgOrganizationConsolidate_sortdir'})
    org_sortkey_tag = invoice_soup.find_all(attrs ={'id': '_ctl0:bc:dgOrganizationConsolidate_sortkey'})
    org_page_index_tag = invoice_soup.find_all(attrs = {'id': '_ctl0:bc:dgOrganizationConsolidate_currentPageIndex'})
    
    
    event_target_output = "_ctl0:bc:btnDownload"
    view_state_output = view_state_tag[0]['value']
    view_state_gen_output = view_state_gen_tag[0]['value']
    view_state_encrypt_output = view_state_encrypt_tag[0]['value']
    event_validation_output = event_validation_tag[0]['value']
    #month_val = str(month) # add value from loop?
    #year_val = str(year) # add value from loop
    sortdir_val = 'Ascending'
    
    
    invoice_params = {"__EVENTTARGET": '_ctl0:bc:btnDownload',\
                    "__EVENTARGUMENT":"",\
                    "__VIEWSTATE":f'{view_state_output}',\
                    "__VIEWSTATEGENERATOR":f'{view_state_gen_output}',\
                    "__VIEWSTATEENCRYPTED":f'{view_state_encrypt_output}',\
                    "__EVENTVALIDATION":f'{event_validation_output}',\
                    '_ctl0:bc:Month': str(month),\
                    '_ctl0:bc:Year': str(year),\
                    '_ctl0:bc:dgOrganizationConsolidate_sortdir': 'Ascending',\
                    '_ctl0:bc:dgOrganizationConsolidate_sortkey': "",\
                    '_ctl0:bc:dgOrganizationConsolidate_currentPageIndex': '0'}   
    
    return invoice_params

# COMMAND ----------

# create url for redirecting to data extraction point
def get_target_url(reseller_site): # removed month, year and reseller_id
  concat = 'https://'+ reseller_site + '/Reseller/default.aspx?enc2=MMqzaxyRKofdit-LY5sXcHadZUa4li09oMKbyWiCYYn6ceFs4Jc7qhNCsh_cDjIEspp6aB1WzRonLea-eKvRhreUCxVhJQPNId_M5N0xrImsP11f71AnKmw03ePNmEbP'
  return concat

# COMMAND ----------

# error handling for requests
def url_loop(url, time_out):
    try:
        res = requests.get(url, timeout = time_out)
        print("success! continue to ", url)
    except requests.ConnectionError as e:
        print("OOPS!! Connection Error. Make sure you are connected to Internet. Technical Details given below.\n")
        print("cannot proceed to", url, " skipping to next reseller")
        #print(str(e))          
     #   renewIPadress()
    except requests.Timeout as e:
        print("OOPS!! Timeout Error")
        print("cannot proceed to", url, " skipping to next reseller")
        print(str(e))
     #   renewIPadress()
    except requests.RequestException as e:
        print("OOPS!! General Error")
        print("cannot proceed to", url, " skipping to next reseller")
        print(str(e))
      #  renewIPadress()
    except KeyboardInterrupt:
        print("keyboard error, bye")

# COMMAND ----------

# convert string data output to pandas and then convert as a spark dataframe
def string_to_df(client_data, year, month):
  print(client_data)
  #string = client_data.content
  df = pd.read_csv(io.StringIO((client_data.content).decode('utf-8')),sep=',')
  print("read client csv df:", df)
  
  # add new fields to the string with headers
  df['Billing_Year'] = str(year)
  df['Billing_Month'] = str(month)
  #df['Extract_Timestamp']= date_time_obj.strftime('%Y-%m-%d %H:%M:%S')
  
  # add pretty function to remove unnamed columns for pandas df
  df1 = df.loc[:, ~df.columns.str.contains('^Unnamed')]
  
  # convert pandas to spark df and return this
  spark_df = spark.createDataFrame(df1)
  print("spark df: ")
  
  return spark_df

# COMMAND ----------

# merge schema for column differences
def spark_schema_merge(df_master,df):
  cols = df_master.columns
  for column in [column for column in df_master.columns if column not in df.columns]:
    df = df.withColumn(column, lit(None))
  union_df = df.unionByName(df_master, allowMissingColumns = True)
  merged_df = union_df.select(cols)
  return merged_df

# COMMAND ----------

def spark_dtype(df):

  df2 = df.withColumn("CustomerID", F.col("CustomerID").cast(StringType()))\
          .withColumn("OrganizationName", F.col("OrganizationName").cast(StringType()))\
          .withColumn("OrganizationReferenceID", F.col("OrganizationReferenceID").cast(StringType()))\
          .withColumn("SaleCode", F.col("SaleCode").cast(StringType()))\
          .withColumn("MonthlyMin", F.round("MonthlyMin",2).cast(FloatType()))\
          .withColumn("volume", F.col('volume').cast(IntegerType()))\
          .withColumn("TierPrice", F.col("TierPrice").cast(StringType()))\
          .withColumn("XAPerApp", F.col("XAPerApp").cast(StringType()))\
          .withColumn("TotalXA", F.col('TotalXA').cast(IntegerType()))\
          .withColumn("BTotalXA", F.round("BTotalXA",2).cast(FloatType()))\
          .withColumn("ENonBusinessXA", F.col("ENonBusinessXA").cast(StringType()))\
          .withColumn("BusinessXA", F.col('BusinessXA').cast(IntegerType()))\
          .withColumn("BBusinessXA", F.round("BBusinessXA",2).cast(FloatType()))\
          .withColumn("EBusinessXA", F.col("EBusinessXA").cast(StringType()))\
          .withColumn("TotalLoans", F.col('TotalLoans').cast(IntegerType()))\
          .withColumn("BTotalLoans", F.round("BTotalLoans",2).cast(FloatType()))\
          .withColumn("BL", F.col('BL').cast(IntegerType()))\
          .withColumn("BBL", F.round("BBL",2).cast(FloatType()))\
          .withColumn("EBL", F.col("EBL").cast(StringType()))\
          .withColumn("SBA_BL", F.col('SBA_BL').cast(IntegerType()))\
          .withColumn("BSBA_BL", F.round("BSBA_BL",2).cast(FloatType()))\
          .withColumn("ESBA_BL", F.col("ESBA_BL").cast(StringType()))\
          .withColumn("CC", F.col('CC').cast(IntegerType()))\
          .withColumn("BCC", F.round("BCC",2).cast(FloatType()))\
          .withColumn("HE", F.col('HE').cast(IntegerType()))\
          .withColumn("BHE", F.round("BHE",2).cast(FloatType()))\
          .withColumn("EHE", F.col("EHE").cast(StringType()))\
          .withColumn("ML", F.col('ML').cast(IntegerType()))\
          .withColumn("BML", F.round("BML",2).cast(FloatType()))\
          .withColumn("EML", F.col("EML").cast(StringType()))\
          .withColumn("PL", F.col('PL').cast(IntegerType()))\
          .withColumn("BPL", F.round("BPL",2).cast(FloatType()))\
          .withColumn("LIFESTYLE_PL", F.col('LIFESTYLE_PL').cast(IntegerType()))\
          .withColumn("BLIFESTYLE_PL", F.col('BLIFESTYLE_PL',2).cast(FloatType()))\
          .withColumn("ELIFESTYLE_PL", F.col('ELIFESTYLE_PL').cast(StringType()))\
          .withColumn("VL", F.col('VL').cast(IntegerType()))\
          .withColumn("BVL", F.round("BVL",2).cast(FloatType()))\
          .withColumn("EVL", F.col("EVL").cast(StringType()))\
          .withColumn("CUDL_VL", F.col('CUDL_VL').cast(IntegerType()))\
          .withColumn("BCUDL_VL", F.round("BCUDL_VL",2).cast(FloatType()))\
          .withColumn("ECUDL_VL", F.col("ECUDL_VL").cast(StringType()))\
          .withColumn("CUDC_VL", F.col('CUDC_VL').cast(IntegerType()))\
          .withColumn("BCUDC_VL", F.round("BCUDC_VL",2).cast(FloatType()))\
          .withColumn("ECUDC_VL", F.col("ECUDC_VL").cast(StringType()))\
          .withColumn("ROUTEONE_VL", F.col('ROUTEONE_VL').cast(IntegerType()))\
          .withColumn("BROUTEONE_VL", F.round("BROUTEONE_VL",2).cast(FloatType()))\
          .withColumn("EROUTEONE_VL", F.col("EROUTEONE_VL").cast(StringType()))\
          .withColumn("DEALERTRACK_VL", F.col('DEALERTRACK_VL').cast(IntegerType()))\
          .withColumn("BDEALERTRACK_VL", F.round("BDEALERTRACK_VL",2).cast(FloatType()))\
          .withColumn("EDEALERTRACK_VL", F.col("EDEALERTRACK_VL").cast(StringType()))\
          .withColumn("BROUTEONE_VL_VENDOR_FEE", F.round('BROUTEONE_VL_VENDOR_FEE',2).cast(FloatType()))\
          .withColumn("BDEALERTRACK_VL_VENDOR_FEE", F.round("BDEALERTRACK_VL_VENDOR_FEE",2).cast(FloatType()))\
          .withColumn("TotalIDV", F.col('TotalIDV').cast(IntegerType()))\
          .withColumn("BTotalIDV", F.round("BTotalIDV",2).cast(FloatType()))\
          .withColumn("XP_AS3", F.col('XP_AS3').cast(IntegerType()))\
          .withColumn("BXP_AS3", F.round("BXP_AS3",2).cast(FloatType()))\
          .withColumn("XP_AS2", F.col('XP_AS2').cast(IntegerType()))\
          .withColumn("BXP_AS2", F.round("BXP_AS2",2).cast(FloatType()))\
          .withColumn("XP_AS3_SA", F.col('XP_AS3_SA').cast(IntegerType()))\
          .withColumn("BXP_AS3_SA", F.round("BXP_AS3_SA",2).cast(FloatType()))\
          .withColumn("TU_IDV", F.col('TU_IDV').cast(IntegerType()))\
          .withColumn("BTU_IDV", F.round("BTU_IDV",2).cast(FloatType()))\
          .withColumn("TU_IDA", F.col('TU_IDA').cast(IntegerType()))\
          .withColumn("BTU_IDA", F.round("BTU_IDA",2).cast(FloatType()))\
          .withColumn("EF_IDComp", F.col('EF_IDComp').cast(IntegerType()))\
          .withColumn("BEF_IDComp", F.round("BEF_IDComp",2).cast(FloatType()))\
          .withColumn("EF_IDV", F.col('EF_IDV').cast(IntegerType()))\
          .withColumn("BEF_IDV", F.round("BEF_IDV",2).cast(FloatType()))\
          .withColumn("EF_RB", F.col('EF_RB').cast(IntegerType()))\
          .withColumn("BEF_RB", F.round("BEF_RB",2).cast(FloatType()))\
          .withColumn("OFAC", F.col('OFAC').cast(IntegerType()))\
          .withColumn("BOFAC", F.round("BOFAC",2).cast(FloatType()))\
          .withColumn("CRDCTrans", F.col('CRDCTrans').cast(IntegerType()))\
          .withColumn("BCRDCTrans", F.round("BCRDCTrans",2).cast(FloatType()))\
          .withColumn("ACHTrans", F.col('ACHTrans').cast(IntegerType()))\
          .withColumn("BACHTrans", F.round("BACHTrans",2).cast(FloatType()))\
          .withColumn("KBB", F.col('KBB').cast(IntegerType()))\
          .withColumn("BKBB", F.round("BKBB",2).cast(FloatType()))\
          .withColumn("NADA", F.col('NADA').cast(IntegerType()))\
          .withColumn("BNADA", F.round("BNADA",2).cast(FloatType()))\
          .withColumn("NADAOther", F.col('NADAOther').cast(IntegerType()))\
          .withColumn("BNADAOther", F.round("BNADAOther",2).cast(FloatType()))\
          .withColumn("BlackBook", F.col('BlackBook').cast(IntegerType()))\
          .withColumn("BBlackBook", F.round("BBlackBook",2).cast(FloatType()))\
          .withColumn("AutoCheck", F.col('AutoCheck').cast(IntegerType()))\
          .withColumn("BAutoCheck", F.round("BAutoCheck",2).cast(FloatType()))\
          .withColumn("Fax", F.col('Fax').cast(IntegerType()))\
          .withColumn("BFax", F.round("BFax",2).cast(FloatType()))\
          .withColumn("AttachedDoc", F.col('AttachedDoc').cast(IntegerType()))\
          .withColumn("BAttachedDoc", F.round("BAttachedDoc",2).cast(FloatType()))\
          .withColumn("MailedDoc", F.col('MailedDoc').cast(IntegerType()))\
          .withColumn("BMailedDoc", F.round("BMailedDoc",2).cast(FloatType()))\
          .withColumn("AVS", F.col('AVS').cast(IntegerType()))\
          .withColumn("BAVS", F.round("BAVS",2).cast(FloatType()))\
          .withColumn("PRECISEID", F.col('PRECISEID').cast(IntegerType()))\
          .withColumn("BPRECISEID", F.round("BPRECISEID",2).cast(FloatType()))\
          .withColumn("PRECISEID_KIQ", F.col('PRECISEID_KIQ').cast(IntegerType()))\
          .withColumn("BPRECISEID_KIQ", F.round("BPRECISEID_KIQ",2).cast(FloatType()))\
          .withColumn("PreciseIDCrossCore", F.col('PreciseIDCrossCore').cast(IntegerType()))\
          .withColumn("BPreciseIDCrossCore", F.round("BPreciseIDCrossCore",2).cast(FloatType()))\
          .withColumn("PreciseID_KIQCrossCore", F.col('PreciseID_KIQCrossCore').cast(IntegerType()))\
          .withColumn("BPreciseID_KIQCrossCore", F.round("BPreciseID_KIQCrossCore",2).cast(FloatType()))\
          .withColumn("PreciseIDCrossCore_PreScreen", F.col('PreciseIDCrossCore_PreScreen').cast(IntegerType()))\
          .withColumn("BPreciseIDCrossCore_PreScreen", F.round("BPreciseIDCrossCore_PreScreen",2).cast(FloatType()))\
          .withColumn("TUIDVision", F.col('TUIDVision').cast(IntegerType()))\
          .withColumn("BTUIDVision", F.round("BTUIDVision",2).cast(FloatType()))\
          .withColumn("TUIDVisionWithKBA", F.col('TUIDVisionWithKBA').cast(IntegerType()))\
          .withColumn("BTUIDVisionWithKBA", F.round("BTUIDVisionWithKBA",2).cast(FloatType()))\
          .withColumn("EWS", F.col('EWS').cast(IntegerType()))\
          .withColumn("BEWS", F.round("BEWS",2).cast(FloatType()))\
          .withColumn("EWSMatch", F.col('EWSMatch').cast(IntegerType()))\
          .withColumn("BEWSMatch", F.round("BEWSMatch",2).cast(FloatType()))\
          .withColumn("TeleCheck", F.col('TeleCheck').cast(IntegerType()))\
          .withColumn("BTeleCheck", F.round("BTeleCheck",2).cast(FloatType()))\
          .withColumn("DeluxeIDA", F.col('DeluxeIDA').cast(IntegerType()))\
          .withColumn("BDeluxeIDA", F.round("BDeluxeIDA",2).cast(FloatType()))\
          .withColumn("DeluxeDetect", F.col('DeluxeDetect').cast(IntegerType()))\
          .withColumn("BDeluxeDetect", F.round("BDeluxeDetect",2).cast(FloatType()))\
          .withColumn("RSAICHECK", F.col('RSAICHECK').cast(IntegerType()))\
          .withColumn("BRSAICHECK", F.round("BRSAICHECK",2).cast(FloatType()))\
          .withColumn("RSAIAUTHEN", F.col('RSAIAUTHEN').cast(IntegerType()))\
          .withColumn("BRSAIAUTHEN", F.round("BRSAIAUTHEN",2).cast(FloatType()))\
          .withColumn("OdysseyAutoFax", F.col('OdysseyAutoFax').cast(IntegerType()))\
          .withColumn("BOdysseyAutoFax", F.round("BOdysseyAutoFax",2).cast(FloatType()))\
          .withColumn("OtherFeeCost", F.round("OtherFeeCost",2).cast(FloatType()))\
          .withColumn("OtherFeeDescription", F.col("OtherFeeDescription").cast(StringType()))\
          .withColumn("SMS", F.col('SMS').cast(IntegerType()))\
          .withColumn("CarFax", F.col('CarFax').cast(IntegerType()))\
          .withColumn("CONSUMER_MODULE", F.col('CONSUMER_MODULE').cast(IntegerType()))\
          .withColumn("BCONSUMER_MODULE", F.round("BCONSUMER_MODULE",2).cast(FloatType()))\
          .withColumn("INDIRECT_LENDING_VL", F.col('INDIRECT_LENDING_VL').cast(IntegerType()))\
          .withColumn("BINDIRECT_LENDING_VL", F.round("BINDIRECT_LENDING_VL",2).cast(FloatType()))\
          .withColumn("BL_MODULE", F.col('BL_MODULE').cast(IntegerType()))\
          .withColumn("BBL_MODULE", F.round("BBL_MODULE",2).cast(FloatType()))\
          .withColumn("HE_MODULE", F.col('HE_MODULE').cast(IntegerType()))\
          .withColumn("BHE_MODULE", F.round("BHE_MODULE",2).cast(FloatType()))\
          .withColumn("TotalAmountDue", F.round("TotalAmountDue",2).cast(FloatType()))\
          .withColumn("Billing_Year", F.col('Billing_Year').cast(IntegerType()))\
          .withColumn("Billing_Month", F.col('Billing_Month').cast(IntegerType()))\
          .withColumn("ExtractRunTime", F.unix_timestamp("ExtractRunTime", 'MM-dd-yyyy HH:mm:SS').cast(TimestampType()))\
  
  return df2

# COMMAND ----------

def create_file_name(reseller, month, year):
  # creates file name with reseller, year, mont of data, and adds timestamp to the name of file
  # TO DO : add code to get reseller name without spaces
  import datetime
  now = datetime.datetime.now()
  file_name = "Monthly_" + reseller.replace(" ","") + "_BillDate_" + str(year) + '-'+ str(format(month).zfill(2)) + "_ExtractTS_" + str(datetime.datetime.now()).replace(' ','_').replace(":",'') + '.csv'
  return file_name

# COMMAND ----------

# input to make_session takes the WebLogin info, login_url mainly to generate the soup content
# outputs a csv with 3 new fields added
def extract_data(login_url, target_url, login_cred, year, month, TimeOut): # add back  csv_file between login and year?
  from bs4 import BeautifulSoup as bs
  with requests.session() as s:
    print("new session opened")
    headers = {'User-Agent': 'Mozilla/5.0 (Windows NT 6.1; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/56.0.2924.76 Safari/537.36',\
               "Upgrade-Insecure-Requests": "1",\
               "DNT": "1",\
               "Accept": "text/html,application/xhtml+xml,application/xml;q=0.9,*/*;q=0.8",\
               "Accept-Language": "en-US,en;q=0.5","Accept-Encoding": "gzip, deflate"}
    get_link = s.get(login_url, timeout = TimeOut, headers = headers, verify = False) # pass get_link into get_hidden function as input

    modified_params = get_hidden(get_link.text, login_cred)
    print("modified_params:", modified_params)
    
    # pass the modified params for POST method and get the reseller data from client API call
    reseller = s.post(login_url, modified_params, headers = headers, verify = False)
    print("reseller post method:", reseller)
    print(reseller.status_code, ' status for login url')
    
    target_url = 
    # change the reseller target URL for history -- DONE
    print('pointing to target url: ', target_url)
    
    # change headers
    headers = {"Accept": "text/html,application/xhtml+xml,application/xml;q=0.9,image/avif,image/webp,image/apng,*/*;q=0.8,application/signed-exchange;v=b3;q=0.9",\
               "Accept-Encoding": "gzip, deflate, br",\
               "Accept-Language": "en-US,en;q=0.9",\
               "Cache-Control": "max-age=0",\
               "Connection": "keep-alive",\
               "Content-Length": "3596",\
               "Content-Type": "application/x-www-form-urlencoded",\
               "Host": "webloan.additionfi.com",\
               "Origin": "https://webloan.additionfi.com",\
               "Referer": "https://webloan.additionfi.com/reseller/new_invoice_list.aspx?enc2=MMqzaxyRKofdit-LY5sXcHadZUa4li09oMKbyWiCYYn6ceFs4Jc7qhNCsh_cDjIEspp6aB1WzRonLea-eKvRhreUCxVhJQPNId_M5N0xrImsP11f71AnKmw03ePNmEbP",\
               "sec-ch-ua": '" Not;A Brand";v="99", "Google Chrome";v="91", "Chromium";v="91"',\
               "sec-ch-ua-mobile": "?0",\
               "Sec-Fetch-Dest": "document",\
               "Sec-Fetch-Mode": "navigate",\
               "Sec-Fetch-Site": "same-origin",\
               "Sec-Fetch-User": "?1",\
               "Upgrade-Insecure-Requests": "1",\
               "User-Agent": "Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/91.0.4472.101 Safari/537.36"}
    
    invoice_params = get_hidden2()
    
    # call function to requests exceptions
    try:
      file_string = s.get(target_url, allow_redirects = True, headers = headers, verify = False, timeout = TimeOut)
      #r = requests.get(url, timeout=timeout)
      file_string.raise_for_status()
      return file_string
    except requests.exceptions.HTTPError as errh:
      print ("Http Error:", errh)
    except requests.exceptions.ConnectionError as errc:
      print ("Error Connecting:",errc)
    except requests.exceptions.Timeout as errt:
      print ("Timeout Error:", errt)
    except requests.exceptions.RequestException as err:
      print ("Oops: Something Else", err)
    
    if file_string.status_code == 200:
      print(file_string.content)
    else:
      print('status is not 200, file fail')
    
    s.close()
    s.cookies.clear()
    
  return file_string

# COMMAND ----------

# MAGIC %md #begin pipeline

# COMMAND ----------

# MAGIC %md #create reseller dataframe

# COMMAND ----------

sql = """
(SELECT  [reseller_name]
      ,[reseller_login_url]
      ,[reseller_id]
      ,[host]
      ,[public]
  FROM [dbo].[self_hosted_resellers]
  where [active_flag] = 'yes'
  ) as t
  """

# COMMAND ----------

spark = SparkSession.builder.getOrCreate()

df = spark.read\
.format("jdbc")\
.option("url", jdbcUrl)\
.option("driver", jdbcDriver)\
.option("user", username)\
.option("password", password)\
.option("dbtable", sql)\
.load()

# COMMAND ----------

# MAGIC %md #get reseller info

# COMMAND ----------

now = datetime.now(timezone(timedelta(hours=-7)))
time = now.strftime("%H:%M:%S")
date_time = now.strftime("%m-%d-%Y %H:%M:%S")
month = now.month
year = now.year
date_time

# COMMAND ----------



# COMMAND ----------

master_df = spark.read.csv('/mnt/self-hosted/master_billing.csv', header = True)

for i in df.toPandas()[['reseller_name', 'reseller_login_url', 'reseller_id', 'host', 'public']].values:
    print("processing for reseller:", i[0],'\nlogin url: ', i[1],'\npublic:', i[4])
    reseller_name = i[0]
    login_url = i[1]
    reseller_id = i[2]
    reseller_host = i[3]
    reseller_public = i[4]
                
    # condition to reset the proxy environment
    if reseller_public == 'proxy':
        print("proxy added")
        os.environ["HTTP_PROXY"] = "http://cosproxy.meridianlink.com:8080"
        os.environ["HTTPS_PROXY"] = "https://cosproxy.meridianlink.com:8080"
        print(os.environ["HTTP_PROXY"])
        print(os.environ["HTTPS_PROXY"])
    else: 
        print("proxy removed")
        os.environ["HTTP_PROXY"] = ""
        os.environ["HTTPS_PROXY"] = ""
        print(os.environ["HTTP_PROXY"])
        print(os.environ["HTTPS_PROXY"])
                    
    # condition to set the login details
    if reseller_name == 'American Heritage':
        login = ah_login
    elif reseller_name == 'The Golden 1 Credit Union':
        login = gold_login
    else: 
        login = default_login
    
    now = datetime.now(timezone(timedelta(hours=-7)))
    #print(now)

    yr = now.year
    mon = now.month

    for year in range(2018, 2022):
        month = 1
        if year < yr:
            for month in range(1,13):
                now = datetime.now(timezone(timedelta(hours=-7)))
                time = now.strftime("%H:%M:%S")
                date_time = now.strftime("%m-%d-%Y %H:%M:%S")
                
                # create target_url. this URL doesnt need preceding zeroes for month
                target_url = get_target_url(str(month),str(year), reseller_host, reseller_id)
                print("target_url:", target_url)
                            
                # string-ify year and month for use
                bill_year = str(year).zfill(4)
                bill_month = str(month).zfill(2)
                # create target file name to store -- not sure whether this is even needed 
                output_csv_file = create_file_name(reseller_name, bill_month, bill_year)
                csv_dir = '/mnt/self-hosted/csv'
                output_csv_path = os.path.join(csv_dir, output_csv_file)
                            
                # create login, and modified parameters
                #print("login:",login)
                string = extract_data(login_url, target_url, login, year, month, time_out)
                
                # add condition for break loop
                if type(string) == type(None):
                  error_reseller.append(reseller_name)
                  continue
                else:
                  pass
                              
                # need to run string_to_df to convert the binary output to a spark df
                df = string_to_df(string, year, month)
                            
                # run schema merge with master set
                merged_df = spark_schema_merge(master_df, df)

                # add new column for all the rows to have same extract run time & display merged_df
                print("merged spark df \n")
                merged_df1 = merged_df.withColumn("ExtractRunTime",lit(date_time))
                
                # create spark DF with the data types incorporated
                final_df = spark_dtype(merged_df1)
                            
                #write the dataframe into a sql table
                final_df1.write.mode("append")\
                    .format("jdbc")\
                    .option("maxStrLength", 4000)\
                    .option("url", jdbcUrl)\
                    .option("user", username)\
                    .option("password", password)\
                    .option("useAzureMSI", "true")\
                    .option("dbtable", table_name)\
                    .save()
                
                print("table updated with", reseller_name)
                            
                # write the spark df to blob as a backup csv
                merged_df.write.option("header", True)\
                    .option("delimiter", ",")\
                    .csv(output_csv_path)
                            
                print("csv saved to blob for ", reseller_name)
                print(f'\n====================================================================\n\n{year}, {month} processed for {reseller_name}\n\n====================================================================')
            month += 1
        elif year == yr:
            for month in range(1, mon):
              # create target_url. this doesnt need preceding zeroes for month
              target_url = get_target_url(str(month),str(year), reseller_host, reseller_id)
              print("target_url:", target_url)

              # string-ify year and month for use
              bill_year = str(year).zfill(4)
              bill_month = str(month).zfill(2)
              # create target file name to store -- not sure whether this is even needed 
              output_csv_file = create_file_name(reseller_name, bill_month, bill_year)
              csv_dir = '/mnt/self-hosted/csv'
              output_csv_path = os.path.join(csv_dir, output_csv_file)

              # create login, and modified parameters
              #print("login:",login)
              string = extract_data(login_url, target_url, login, bill_year, bill_month, time_out) 

              # need to run string_to_df to convert the binary output to a spark df
              df = string_to_df(string, bill_year, bill_month)

              # run schema merge with master set
              merged_df = spark_schema_merge(master_df, df)

              # add new column for all the rows to have same extract run time & display merged_df
              print("merged spark df \n")
              merged_df1 = merged_df.withColumn("ExtractRunTime",lit(date_time))

              #write the dataframe into a sql table
              merged_df1.write.mode("append")\
                  .format("jdbc")\
                  .option("maxStrLength", 4000)\
                  .option("url", jdbcUrl)\
                  .option("user", username)\
                  .option("password", password)\
                  .option("useAzureMSI", "true")\
                  .option("dbtable", table_name)\
                  .save()

              print("table updated with", reseller_name)

              # write the spark df to blob as a backup csv
              merged_df.write.option("header", True)\
                  .option("delimiter", "|")\
                  .csv(output_csv_path)

              print("csv saved to blob for ", reseller_name)
              print(f'\n====================================================================\n\n{year}, {month} processed for {reseller_name}\n\n====================================================================')

# COMMAND ----------

'''
now = datetime.now(timezone(timedelta(hours=-7)))
print(now)

yr = now.year
mon = now.month

for year in range(2018, 2022):
    month = 1
    if year < yr:
        for month in range(1,13):               
            # create target_url. this doesnt need preceding zeroes for month
            target_url = get_target_url(str(month),str(year), reseller_host, reseller_id)
            print("target_url:", target_url)
                        
            # string-ify year and month for use
            bill_year = str(year).zfill(4)
            bill_month = str(month).zfill(2)
            # create target file name to store -- not sure whether this is even needed 
            output_csv_file = create_file_name(reseller_name, bill_month, bill_year)
            csv_dir = '/mnt/self-hosted/csv'
            output_csv_path = os.path.join(csv_dir, output_csv_file)
                        
            # create login, and modified parameters
            #print("login:",login)
            string = extract_data(login_url, target_url, login, bill_year, bill_month, time_out) 
                        
            # need to run string_to_df to convert the binary output to a spark df
            df = string_to_df(string, bill_year, bill_month)
                        
            # run schema merge with master set
            merged_df = spark_schema_merge(master_df, df)
                        
            # write to azure sql
            table_name = "self_hosted_history"

            # add new column for all the rows to have same extract run time & display merged_df
            print("merged spark df \n")
            merged_df1 = merged_df.withColumn("ExtractRunTime",lit(date_time))
                        
            #write the dataframe into a sql table
            merged_df1.write.mode("append")\
                .format("jdbc")\
                .option("maxStrLength", 4000)\
                .option("url", jdbcUrl)\
                .option("user", username)\
                .option("password", password)\
                .option("useAzureMSI", "true")\
                .option("dbtable", table_name)\
                .save()
            
            print("table updated with", reseller_name)
                        
            # write the spark df to blob as a backup csv
            merged_df.write.option("header", True)\
                .option("delimiter", "|")\
                .csv(output_csv_path)
                        
            print("csv saved to blob for ", reseller_name)
            print('reseller processed:', reseller_name)
        month += 1
    elif year == yr:
        for month in range(1, mon + 1):
            # create target_url. this doesnt need preceding zeroes for month
            target_url = get_target_url(str(month),str(year), reseller_host, reseller_id)
            print("target_url:", target_url)
                        
            # string-ify year and month for use
            bill_year = str(year).zfill(4)
            bill_month = str(month).zfill(2)
            # create target file name to store -- not sure whether this is even needed 
            output_csv_file = create_file_name(reseller_name, bill_month, bill_year)
            csv_dir = '/mnt/self-hosted/csv'
            output_csv_path = os.path.join(csv_dir, output_csv_file)
                        
            # create login, and modified parameters
            #print("login:",login)
            string = extract_data(login_url, target_url, login, bill_year, bill_month, time_out) 
                        
            # need to run string_to_df to convert the binary output to a spark df
            df = string_to_df(string, bill_year, bill_month)
                        
            # run schema merge with master set
            merged_df = spark_schema_merge(master_df, df)
                        
            # write to azure sql
            table_name = "self_hosted_history"

            # add new column for all the rows to have same extract run time & display merged_df
            print("merged spark df \n")
            merged_df1 = merged_df.withColumn("ExtractRunTime",lit(date_time))
                        
            #write the dataframe into a sql table
            merged_df1.write.mode("append")\
                .format("jdbc")\
                .option("maxStrLength", 4000)\
                .option("url", jdbcUrl)\
                .option("user", username)\
                .option("password", password)\
                .option("useAzureMSI", "true")\
                .option("dbtable", table_name)\
                .save()
            
            print("table updated with", reseller_name)
                        
            # write the spark df to blob as a backup csv
            merged_df.write.option("header", True)\
                .option("delimiter", "|")\
                .csv(output_csv_path)
                        
            print("csv saved to blob for ", reseller_name)
            print('reseller processed:', reseller_name)'''

# COMMAND ----------

os.environ["HTTP_PROXY"] = ""
os.environ["HTTPS_PROXY"] = ""
