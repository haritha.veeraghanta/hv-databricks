# Databricks notebook source
# MAGIC %md #import library

# COMMAND ----------

import requests
import pandas as pd
import numpy as np
import pyodbc
from bs4 import BeautifulSoup as bs
import os
import datetime
import requests
import io
from io import StringIO
import pyspark
from pyspark import SparkConf, SparkContext
from pyspark.sql import SparkSession
from pyspark.sql.functions import lit
from requests.exceptions import Timeout

# COMMAND ----------

# MAGIC %md # declare and assign widget

# COMMAND ----------

# declare variables used in the pipeline

dbutils.widgets.text("database", "")
dbutils.widgets.text("server", "")
dbutils.widgets.text("username", "")
dbutils.widgets.text("password", "")
dbutils.widgets.text("target_table", "")

dbutils.widgets.text("storage_account", "")
dbutils.widgets.text("storage_key", "")
dbutils.widgets.text("container", "")

dbutils.widgets.text("txtLogin", "")
dbutils.widgets.text("txtPassword", "")
dbutils.widgets.text("cmdLogin", "")

# special login creds
dbutils.widgets.text("ah_txtLogin", "")
dbutils.widgets.text("ah_txtPassword", "")

dbutils.widgets.text("gold_txtLogin", "")
dbutils.widgets.text("gold_txtPassword", "")

dbutils.widgets.text("timeout","")

# COMMAND ----------

# assign variables from the master run script

database = dbutils.widgets.get("database")
server1 = dbutils.widgets.get("server")
username = dbutils.widgets.get("username")
password = dbutils.widgets.get("password")
table = dbutils.widgets.get("target_table")

st_account = dbutils.widgets.get("storage_account")
st_key = dbutils.widgets.get("storage_key")
st_container = dbutils.widgets.get("container")

webuser = dbutils.widgets.get("txtLogin")
webpwd = dbutils.widgets.get("txtPassword")
#weblogin = dbutils.widgets.get("cmdLogin")

#special login creds
ah_user = dbutils.widgets.get("ah_txtLogin")
ah_pwd = dbutils.widgets.get("ah_txtPassword")

gold_user = dbutils.widgets.get("gold_txtLogin")
gold_pwd = dbutils.widgets.get("gold_txtPassword")
time_out = int(dbutils.widgets.get("timeout"))

# COMMAND ----------

# connect to azure blob
#spark.conf.set("fs.azure.account.key.<your-storage-account-name>.blob.core.windows.net","<your-storage-account-access-key>")

# set the config to read_write
spark.conf.set("fs.azure.account.key.{0}.blob.core.windows.net".format(st_account),st_key)

output_container_path = "wasbs://%s@%s.blob.core.windows.net" % (st_container, st_account)
print(output_container_path)
output_blob_folder = "%s/self-hosted" % output_container_path
print(output_blob_folder)

# COMMAND ----------

default_login = {'txtLogin':webuser,'txtPassword':webpwd,'cmdLogin':'Login'}
# special login
ah_login = {'txtLogin': ah_user, 'txtPassword': ah_pwd, 'cmdLogin':'Login'}
gold_login = {'txtLogin': gold_user, 'txtPassword': gold_pwd, 'cmdLogin': 'Login'}

# COMMAND ----------

server =  server1.replace(".privatelink","")

jdbcUrl = f"jdbc:sqlserver://{server}.database.windows.net:1433;database={database};user={username};password={password};encrypt=true;trustServerCertificate=false;hostNameInCertificate=*.database.windows.net;loginTimeout=30"
jdbcDriver = "com.microsoft.sqlserver.jdbc.SQLServerDriver"

#url = f"jdbc:sqlserver://{server};database={database}"

# COMMAND ----------

print(database, server, username, password, webuser, webpwd, ah_user, ah_pwd, gold_user, gold_pwd, st_account, st_key, st_container)

# COMMAND ----------

# MAGIC %md # mount azure blob container

# COMMAND ----------

# check if mount is present:
def sub_unmount(str_path):
    if any(mount.mountPoint == str_path for mount in dbutils.fs.mounts()):
        dbutils.fs.unmount(str_path)

        
# no need to run for the test, it is already mounted
'''if (sub_unmount('/mnt/self-hosted'),True):
  # mount storage to cluster
  dbutils.fs.mount(
    source = 'wasbs://{0}@{1}.blob.core.windows.net'.format(st_container,st_account),
    mount_point = '/mnt/self-hosted',
    extra_configs = {'fs.azure.account.key.{0}.blob.core.windows.net'.format(st_account):st_key})

print(st_account,st_key,st_container)'''

# COMMAND ----------

# master_billing csv is provided as the one with superset of columns includes bill month and bill year.

df = spark.read.csv('/mnt/self-hosted/master_billing.csv', header=True)

# COMMAND ----------

# MAGIC %md # functions

# COMMAND ----------

def get_hidden(html,weblogin):
  soup = bs(html,'html.parser')
  event_target_tag = soup.find_all(attrs={'id':'__EVENTTARGET'})
  event_argument_tag = soup.find_all(attrs={'id':"__EVENTARGUMENT"})
  view_state_tag = soup.find_all(attrs={'id':"__VIEWSTATE"})
  view_state_gen_tag = soup.find_all(attrs={'id':"__VIEWSTATEGENERATOR"})
  view_state_encrypt_tag = soup.find_all(attrs={'id':"__VIEWSTATEENCRYPTED"})
  event_validation_tag = soup.find_all(attrs={'id':"__EVENTVALIDATION"})
    
  # get values from tags for hidden
  #event_target_output = event_target_tag[0]['value']
  #event_argument_output = event_argument_tag[0]['value']
  view_state_output = view_state_tag[0]['value']
  view_state_gen_output = view_state_gen_tag[0]['value']
  view_state_encrypt_output = view_state_encrypt_tag[0]['value']
  event_validation_output = event_validation_tag[0]['value']
  
  hidden_params = {'__EVENTTARGET':"", "__EVENTARGUMENT":"", "__VIEWSTATE":f'{view_state_output}', "__VIEWSTATEGENERATOR":f'{view_state_gen_output}', "__VIEWSTATEENCRYPTED":f'{view_state_encrypt_output}', "__EVENTVALIDATION":f'{event_validation_output}'}
  
  hidden_params.update(weblogin)
  print("hidden + login params:", hidden_params)
  return hidden_params

# COMMAND ----------

# create url for redirecting to data extraction point
def get_target_url(month, year,reseller_site,reseller_id):
  concat = 'https://'+ reseller_site + '/inc/PrintPreview/printview_frame.aspx?body_url=/Reseller/monthlyBillingExport.aspx%3FMonth%3D' + str(month) + '%26Year%3D' + str(year) + '%26ResellerID%3D' + reseller_id
  concat1 = concat.replace(" ","")
  return concat1

# COMMAND ----------

# error handling for requests
def url_loop(url, TimeOut):
    try:
        res = requests.get(url, timeout = TimeOut)
        print("success! continue to ", url)
    except requests.ConnectionError as e:
        print("OOPS!! Connection Error. Make sure you are connected to Internet. Technical Details given below.\n")
        print("cannot proceed to", url, "\n skipping to next reseller")
        #print(str(e))          
     #   renewIPadress()
    except requests.Timeout as e:
        print("OOPS!! Timeout Error")
        print("cannot proceed to", url, " skipping to next reseller")
        print(str(e))
     #   renewIPadress()
    except requests.RequestException as e:
        print("OOPS!! General Error")
        print("cannot proceed to", url, " skipping to next reseller")
        print(str(e))
      #  renewIPadress()
    except KeyboardInterrupt:
        print("keyboard error, bye")

# COMMAND ----------

# convert string data output to pandas and then convert as a spark dataframe
def string_to_df(client_data, year, month):
  print(client_data)
  #string = client_data.content
  df = pd.read_csv(io.StringIO((client_data.content).decode('utf-8')),sep=',')
  print("read client csv df:", df)
  
  # add new fields to the string with headers
  df['Billing_Year'] = year
  df['Billing_Month'] = month
  #df['Extract_Timestamp']= date_time_obj.strftime('%Y-%m-%d %H:%M:%S')
  
  # add pretty function to remove unnamed columns for pandas df
  df1 = df.loc[:, ~df.columns.str.contains('^Unnamed')]
  
  # convert pandas to spark df and return this
  spark_df = spark.createDataFrame(df1)
  print("spark df: ")
  
  return spark_df

# COMMAND ----------

# merge schema for column differences
def spark_schema_merge(df_master,df):
  cols = df_master.columns
  for column in [column for column in df_master.columns if column not in df.columns]:
    df = df.withColumn(column, lit(None))
  union_df = df.unionByName(df_master, allowMissingColumns = True)
  merged_df = union_df.select(cols)
  return merged_df

# COMMAND ----------

def create_file_name(reseller, month, year):
  # creates file name with reseller, year, mont of data, and adds timestamp to the name of file
  # TO DO : add code to get reseller name without spaces
  import datetime
  now = datetime.datetime.now()
  file_name = reseller.replace(" ","") + "_BillDate_" + str(year) + '-'+ str(format(month).zfill(2)) + "_ExtractDate_" + str(now.year) + '-'+str(format(now.month).zfill(2))+'-'+str(now.hour)+'-'+str(now.minute)+'.csv'
  return file_name

# COMMAND ----------

# input to make_session takes the WebLogin info, login_url mainly to generate the soup content
# outputs a csv with 3 new fields added
def extract_data(login_url, target_url, login_cred, year, month, TimeOut): # add back  csv_file between login and year?
  from bs4 import BeautifulSoup as bs
  with requests.session() as s:
    print("new session opened")
    headers = headers = {'User-Agent': 'Mozilla/5.0 (Windows NT 6.1; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/56.0.2924.76 Safari/537.36', "Upgrade-Insecure-Requests": "1","DNT": "1","Accept": "text/html,application/xhtml+xml,application/xml;q=0.9,*/*;q=0.8","Accept-Language": "en-US,en;q=0.5","Accept-Encoding": "gzip, deflate"}
    get_link = s.get(login_url, timeout = TimeOut, headers = headers, verify = False) # pass get_link into get_hidden function as input

    modified_params = get_hidden(get_link.text, login_cred)
    print("modified_params:", modified_params)
    
    # pass the modified params for POST method and get the reseller data from client API call
    reseller = s.post(login_url, modified_params, headers = headers, verify = False)
    print("reseller post method:", reseller)
    print(reseller.status_code, ' status for login url')
    
    print('pointing to target url: ', target_url)
    
    # call function to requests exceptions
    #url_loop(target_url, timeout = TimeOut)
    
    file_string = s.get(target_url, allow_redirects = True, headers = headers, verify = False)
    
    if file_string.status_code == 200:
      print(file_string.content)
    else:
      print('status is not 200, file fail')
    s.close()
    s.cookies.clear()
    # add time out or break here !

  return file_string

# COMMAND ----------

# MAGIC %md #begin pipeline

# COMMAND ----------

# MAGIC %md #create reseller dataframe

# COMMAND ----------

sql = """
(SELECT  [reseller_name]
      ,[reseller_login_url]
      ,[reseller_id]
      ,[host]
      ,[public]
  FROM [dbo].[self_hosted_resellers]
  where [public] != '-'
  ) as t
  """

# COMMAND ----------

spark = SparkSession.builder.getOrCreate()

df = spark.read\
.format("jdbc")\
.option("url", jdbcUrl)\
.option("driver", jdbcDriver)\
.option("user", username)\
.option("password", password)\
.option("dbtable", sql)\
.load()

# COMMAND ----------

# MAGIC %md #get reseller info

# COMMAND ----------

import datetime

now = datetime.datetime.now()
time = now.strftime("%H:%M:%S")
date_time = now.strftime("%m/%d/%Y, %H:%M:%S")
month = now.month
year = now.year

# COMMAND ----------

# add spark replace to get reseller info

master_df = spark.read.csv('/mnt/self-hosted/master_billing.csv', header = True)
for i in df.toPandas()[['reseller_name', 'reseller_login_url', 'reseller_id', 'host', 'public']].values:
  print("processing for reseller:", i[0],'\nlogin url: ', i[1],'\npublic:', i[4])
  reseller_name = i[0]
  login_url = i[1]
  reseller_id = i[2]
  reseller_host = i[3]
  reseller_public = i[4]
  
  # condition to reset the proxy environment
  if reseller_public == 'proxy':
    print("proxy added")
    os.environ["HTTP_PROXY"] = "http://cosproxy.meridianlink.com:8080"
    os.environ["HTTPS_PROXY"] = "https://cosproxy.meridianlink.com:8080"
    print(os.environ["HTTP_PROXY"])
    print(os.environ["HTTPS_PROXY"])
  else: 
    print("proxy removed")
    os.environ["HTTP_PROXY"] = ""
    os.environ["HTTPS_PROXY"] = ""
    print(os.environ["HTTP_PROXY"])
    print(os.environ["HTTPS_PROXY"])
    
  # condition to set the login details
  if reseller_name == 'American Heritage':
    login = ah_login
  elif reseller_name == 'The Golden 1 Credit Union':
    login = gold_login
  else: 
    login = default_login
    
  # create target_url. this doesnt need preceding zeroes for month
  target_url = get_target_url(str(month),str(year), reseller_host, reseller_id)
  print("target_url:", target_url)
  
  # string-ify year and month for use
  bill_year = str(year).zfill(4)
  bill_month = str(month).zfill(2)
  # create target file name to store -- not sure whether this is even needed 
  output_csv_file = create_file_name(reseller_name, bill_month, bill_year)
  csv_dir = '/mnt/self-hosted/csv'
  output_csv_path = os.path.join(csv_dir, output_csv_file)
  
  # create login, and modified parameters
  #print("login:",login)
  string = extract_data(login_url, target_url, login, bill_year, bill_month, time_out) 
  
  # need to run string_to_df to convert the binary output to a spark df
  df = string_to_df(string, bill_year, bill_month)
  
  # run schema merge with master set
  merged_df = spark_schema_merge(master_df, df)
  
  # write to azure sql
  table_name = "self_hosted_history"
  #url = f"jdbc:sqlserver://{server};database={database}"

  # add new column for all the rows to have same extract run time
  # display merged_df
  print("merged spark df \n")
  merged_df1 = merged_df.withColumn("ExtractRunTime",lit(date_time))
  
  #write the dataframe into a sql table
  merged_df1.write.mode("append")\
                 .format("jdbc")\
                 .option("maxStrLength", 4000)\
                 .option("url", jdbcUrl)\
                 .option("user", username)\
                 .option("password", password)\
                 .option("useAzureMSI", "true")\
                 .option("dbtable", table_name)\
                 .save()
  print("table updated with", reseller_name)
  
  # write the spark df to blob as a backup csv
  merged_df.write.option("header", True)\
                 .option("delimiter", "|")\
                 .csv(output_csv_path)
  
  print("csv saved to blob for ", reseller_name)
  print('reseller processed:', reseller_name)

# COMMAND ----------

os.environ["HTTP_PROXY"] = ""
os.environ["HTTPS_PROXY"] = ""

# COMMAND ----------

# MAGIC %sh
# MAGIC echo "http proxy is:" $HTTP_PROXY
# MAGIC echo "https proxy is:" $HTTPS_PROXY

# COMMAND ----------

# add script to update the reseller table with last run date join on each reseller name
